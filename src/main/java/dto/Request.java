package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {

    private String idRequestBooking;
    private String idPlatform;
    private String namaPlatform;
    private String docType;
    private String termOfPayment;
}
