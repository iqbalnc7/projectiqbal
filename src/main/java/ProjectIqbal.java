import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectIqbal {
    public static void main(String[] args) {
        SpringApplication.run(ProjectIqbal.class, args);
    }
}
