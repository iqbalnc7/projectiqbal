package repositories;

import model.IqbalNurcahyadi;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IqbalNurcahyadiRepositories extends JpaRepository<IqbalNurcahyadi,String> {
    IqbalNurcahyadi findAllById (String idRequestBooking);
    List<IqbalNurcahyadi> findAll();
}
