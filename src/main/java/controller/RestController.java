package controller;


import dto.Request;
import model.IqbalNurcahyadi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import repositories.IqbalNurcahyadiRepositories;
import service.IqbalNurcahyadiService;

import java.util.List;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {

    @Autowired
    IqbalNurcahyadiRepositories iqbalNurcahyadiRepositories;
    @Autowired
    IqbalNurcahyadiService iqbalNurcahyadiService;

    @GetMapping(value = "/getall")
    public List<IqbalNurcahyadi> getAll(){
        return iqbalNurcahyadiService.getAllData();
    }

    @GetMapping(value = "/getbyid/{id}")
    public IqbalNurcahyadi getById(@PathVariable String id){
        return iqbalNurcahyadiService.getById(id);
    }

    @PostMapping(value = "/post")
    public void getById(@RequestBody Request request){
        iqbalNurcahyadiService.saveData(request);
    }

    @PutMapping(value = "/put/{id}")
    public void putData(@PathVariable String id){
        iqbalNurcahyadiService.updateData(id);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteData(@PathVariable String id){
        iqbalNurcahyadiService.deleteData(id);
    }
}
