package model;


import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Data
@Entity
@Table(name = "iqbal_nurcahyadi")
@EntityListeners(AuditingEntityListener.class)
public class IqbalNurcahyadi {

    @Id
    @Column(name = "idrequestbooking", unique = true, nullable = false)
    private String idRequestBooking;

    @Column(name = "id_platform")
    private String idPlatform;

    @Column(name = "nama_platform")
    private String namaPlatform;

    @Column(name = "doc_type")
    private String docType;

    @Column(name = "term_of_payment")
    private String termOfPayment;
}
