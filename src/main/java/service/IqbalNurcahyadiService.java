package service;

import dto.Request;
import model.IqbalNurcahyadi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.IqbalNurcahyadiRepositories;

import java.util.List;

@Service
public class IqbalNurcahyadiService {

    @Autowired
    IqbalNurcahyadiRepositories iqbalNurcahyadiRepositories;

    public List<IqbalNurcahyadi> getAllData(){
        return iqbalNurcahyadiRepositories.findAll();
    }

    public IqbalNurcahyadi getById(String id){
        return iqbalNurcahyadiRepositories.findAllById(id);
    }

    public void saveData(Request request){
        IqbalNurcahyadi iqbalNurcahyadi = new IqbalNurcahyadi();
        iqbalNurcahyadi.setIdPlatform(request.getIdPlatform());
        iqbalNurcahyadi.setDocType(request.getDocType());
        iqbalNurcahyadi.setNamaPlatform(request.getNamaPlatform());
        iqbalNurcahyadi.setTermOfPayment(request.getTermOfPayment());
        iqbalNurcahyadi.setIdRequestBooking(request.getIdRequestBooking());
        iqbalNurcahyadiRepositories.save(iqbalNurcahyadi);
    }

    public void updateData(String id){
        IqbalNurcahyadi iqbalNurcahyadi = iqbalNurcahyadiRepositories.findAllById(id);
        iqbalNurcahyadi.setTermOfPayment("NONE");
        iqbalNurcahyadiRepositories.save(iqbalNurcahyadi);
    }

    public void deleteData(String id){
        IqbalNurcahyadi iqbalNurcahyadi = iqbalNurcahyadiRepositories.findAllById(id);
        iqbalNurcahyadiRepositories.delete(iqbalNurcahyadi);
    }
}
